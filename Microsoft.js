const searchBox = document.getElementById('search-box');
const navi = document.getElementById('hamnav');
const back = document.getElementById('arrow-left');

function toggleSeachBox() {
  searchBox.style.display='flex';
  navi.style.display='none';
}

function closeSearch(){
  searchBox.style.display='none';
  navi.style.display='flex';
}

function backToNav(){
  searchBox.style.display='none';
  navi.style.display='flex';
}